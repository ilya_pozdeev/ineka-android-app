package Android.Ineka;
// Konstantin
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.SwitchCompat;

import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SettingsActivity extends AppCompatActivity {

    String name, surname, eMail, password;
    boolean sound, notification, vibration;
    SwitchCompat nightModeSwitch;
    SharedPreferences sharedPreferences = null;

    EditText nameInput;
    EditText surnameInput;
    EditText eMailInput;
    EditText passwordInput;

    String result;

    private TextView textViewResult;
    private JsonPlaceHolderApi jsonPlaceHolderApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        nameInput = (EditText) findViewById(R.id.editTextPersonName);
        surnameInput = (EditText) findViewById(R.id.editTextPersonSurname);
        eMailInput = (EditText) findViewById(R.id.editTextEmailAddress);
        passwordInput = (EditText) findViewById(R.id.editTextPassword);
        ////////
        nightModeSwitch = findViewById(R.id.switchNight);

        sharedPreferences = getSharedPreferences("night", 0);
        Boolean booleanValue = sharedPreferences.getBoolean("night_mode", true);
        if (booleanValue) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            nightModeSwitch.setChecked(true);
        }
        nightModeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                    nightModeSwitch.setChecked(true);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean("night_mode", true);
                    editor.commit();
                } else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                    nightModeSwitch.setChecked(false);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean("night_mode", false);
                    editor.commit();
                }
            }
        });

        ////////
    }

    public void onClickSaveButton(View view) {
        name = nameInput.getText().toString();
        surname = surnameInput.getText().toString();
        eMail = eMailInput.getText().toString();
        password = passwordInput.getText().toString();

        textViewResult = findViewById(R.id.text_view_result); // This is for testing purposes

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.INEKA_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        result = "Name: " + name + " Surname: " + surname + " E-mail: " + eMail + " Pass: " + password;
//        showResult();

        createPost();
//        getPosts();
//        getComments();

//        Toast.makeText(SettingsActivity.this, "State saved", Toast.LENGTH_SHORT).show();

    }

    private void createPost() {
        Post post = new Post(name, surname, eMail, password);

//        Map<String, String> fields = new HashMap<>();
//        fields.put("userId", "25");
//        fields.put("userId", "New Title");

        Call<Post> call = jsonPlaceHolderApi.createPost(post);

        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                Post postResponse = response.body();

                if (!response.isSuccessful()) {
                    textViewResult.setText("Code: " + response.code());
                    return;
                }
                if (MainActivity.SESSION.equals(postResponse.getSession())) {

//                    MainActivity.SESSION = postResponse.getSession();

                    String content = "";
                    content += "Code: " + response.code() + "\n";
                    content += "Session: " + postResponse.getSession() + "\n";
                    content += "Name: " + postResponse.getName() + "\n";
//                content += "Message: " + postResponse.getMessage() + "\n";

//                System.out.println("!!!!!!!!!!!!!!!!!!" + postResponse.getSession() + "MainActivity session: " + MainActivity.SESSION);
//                content += "Email: " + postResponse.getEmail() + "\n";
//                content += "Text: " + postResponse.getText() + "\n\n";

                    Toast.makeText(SettingsActivity.this, "Your user data is changed", Toast.LENGTH_SHORT).show();
                    textViewResult.setText(content);
                } else {
                    Toast.makeText(SettingsActivity.this, "Your user data is incorrect", Toast.LENGTH_SHORT).show();
                    textViewResult.setText("Your user data is incorrect");
                }
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                textViewResult.setText(t.getMessage());
            }
        });

    }

    private void getPosts() {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("userId", "1");
        parameters.put("_sort", "id");
        parameters.put("_order", "desc");

        //        Post call = new Post();
        Call<List<Post>> call = jsonPlaceHolderApi.getPosts(null, null, null);

        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {

                if(!response.isSuccessful()) {
                    textViewResult.setText("Code: " + response.code());
                    return;
                }
                List<Post> posts = response.body();

                //This returns first line of text for testing purposes
                //{"request_type":"get","name":"Testovich","id":100,"time":"2021/03/18 11:49:31","age":22,"key":"ya_e9on1ka%s#d&jia"}
//                textViewResult.append("ID line 1 = " + posts.get(0).getId());

                for (Post post : posts) {
                    String content = "";
//                    content = "request_type: " + post.getRequest_type() + "\n";
//                    content += "name: " + post.getName() + "\n";
//                    content += "id: " + post.getId() + "\n";
//                    content += "time: " + post.getTime() + "\n";
//                    content += "age: " + post.getAge() + "\n";
//                    content += "key: " + post.getKey() + "\n\n";

//                    content += "ID: " + post.getId() + "\n";
//                    content += "User ID: " + post.getUserId() + "\n";
//                    content += "Title: " + post.getTitle() + "\n";
//                    content += "Text: " + post.getText() + "\n\n";

                    textViewResult.append(content);
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                textViewResult.setText(t.getMessage());
            }
        });
    }

    private void getComments() {
        Call<List<Comment>> call = jsonPlaceHolderApi
                .getComments("https://jsonplaceholder.typicode.com/posts/3/comments");

        call.enqueue(new Callback<List<Comment>>() {
            @Override
            public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {

                if(!response.isSuccessful()) {
                    textViewResult.setText("Code: " + response.code());
                    return;
                }
                List<Comment> comments = response.body();

                for (Comment comment : comments) {
                    String content = "";
                    content += "ID: " + comment.getId() + "\n";
                    content += "Post ID: " + comment.getPostId() + "\n";
                    content += "Name: " + comment.getName() + "\n";
                    content += "Email: " + comment.getEmail() + "\n";
                    content += "Text: " + comment.getText() + "\n\n";

                    textViewResult.append(content);
                }
            }

            @Override
            public void onFailure(Call<List<Comment>> call, Throwable t) {
                textViewResult.setText(t.getMessage());
            }
        });
    }

    private void showResult() {
        Toast.makeText(SettingsActivity.this, result, Toast.LENGTH_SHORT).show();
    }

//    public void onClickNightMode(View view) {
//
//        nightModeSwitch = findViewById(R.id.switchNight);
//
//        sharedPreferences = getSharedPreferences("night", 0);
//        Boolean booleanValue = sharedPreferences.getBoolean("night_mode", true);
//        if (booleanValue) {
//            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
//            nightModeSwitch.setChecked(true);
//        }
//        nightModeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
//                    nightModeSwitch.setChecked(true);
//                    SharedPreferences.Editor editor = sharedPreferences.edit();
//                    editor.putBoolean("night_mode", true);
//                    editor.commit();
//                } else {
//                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
//                    nightModeSwitch.setChecked(false);
//                    SharedPreferences.Editor editor = sharedPreferences.edit();
//                    editor.putBoolean("night_mode", false);
//                    editor.commit();
//                }
//            }
//        });
//
//    }








}