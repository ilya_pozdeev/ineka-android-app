package Android.Ineka;
// Konstantin
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface JsonPlaceHolderApi {

    //    @GET("/json-test/") // relative URL
    @GET("/posts/") // relative URL
    Call<List<Post>> getPosts(
            @Query("userId") Integer [] userId,
            @Query("_sort") String sort,
            @Query("_order") String order
    );

    Call<List<Post>> getPosts(
            @QueryMap Map<String, String> parameters);

    @GET("/posts/{id}/comments") // relative URL
    Call<List<Comment>> getComments(@Path("id") int postId);

    @GET
    Call<List<Comment>> getComments(@Url String url);

    @POST("/registration/")
    Call<Post> createPost(@Body Post post);

    @POST("/user/find/")
    Call<Post> createSearchPost(@Body Post post);

//    @FormUrlEncoded
//    @POST("posts")
//    Call<Post> createPost(
//            @Field("userId") int userId,
//            @Field("title") String title,
//            @Field("body") String text
//    );
//
//    @FormUrlEncoded
//    @POST("posts")
//    Call<Post> createPost(@FieldMap Map<String, String> fields);

}
