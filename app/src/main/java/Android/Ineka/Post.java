package Android.Ineka;
// Konstantin

import com.google.gson.annotations.SerializedName;

public class Post {

    //        @SerializedName("name")
    private String email;

    //        @SerializedName("id")
    private String password;

    //    @SerializedName("time")
    private String name;

    //    @SerializedName("age")
    private String surname;

    private String session;

    private String message;

    private String result;

    public Post(String session, String name, String surname) {
        this.session = session;
        this.name = name;
        this.surname = surname;
    }

    public Post(String name, String surname, String email, String password) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getSession() {
        return session;
    }

    public String getMessage() {
        return message;
    }

    public String getResult() {
        return result;
    }

////    @SerializedName("userId")
//    private int userId; // userId
//
////    @SerializedName("id")
//    private Integer id;
//
////    @SerializedName("title")
//    private String title;
//
//    @SerializedName("body")
//    private String text;
//
//    public Post(int userId, String title, String text) {
//        this.userId = userId;
//        this.title = title;
//        this.text = text;
//    }
//
//    public int getUserId() {
//        return userId;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public String getText() {
//        return text;
//    }
//
//    @Override
//    public String toString() {
//        return "Post{" +
//                "userId=" + userId +
//                ", id=" + id +
//                ", title='" + title + '\'' +
//                ", text='" + text + '\'' +
//                '}';
//    }
}

