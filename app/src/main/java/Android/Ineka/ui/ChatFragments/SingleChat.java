package Android.Ineka.ui.ChatFragments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.ArrayList;
import org.json.JSONObject;
import org.json.JSONException;

import Android.Ineka.MainActivity;
import Android.Ineka.R;
import Android.Ineka.ui.allChats.AllChats;
import Android.Ineka.ui.allChats.AllChatsAdapter;
import Android.Ineka.ui.allChats.SingleChatAdapter;
import Android.Ineka.ui.register.RegisterActivity;

public class SingleChat extends AppCompatActivity {
    static final String SEND_MESSAGE_URL = "/send/message";
    static final String GET_ALL_MESSAGES_URL = "/get/chat/messages";  // msg + result [{a, b}] //
    static final String GET_NEW_MESSAGES_URL = "/get/chat/new-messages";
    public static final String INEKA_URL = "http://eu3inekaserver-env.eba-9mj3emqe.eu-west-1.elasticbeanstalk.com";
    private RecyclerView mRecyclerView;
    private SingleChatAdapter mAdapter;


    private Button SendButton;
   private String userName;
   private EditText textMessage;
   private EditText timeMessage;
   private long messageTime;
   private int ID = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_chat);
        //RecyclerView listView = findViewById(R.id.list_of_messages);
        FloatingActionButton SendButton = findViewById(R.id.btnSend);
        textMessage = (EditText) findViewById(R.id.MsgText);
        setupRecyclerView();
        GetNewMessages();

       SendButton.setOnClickListener(new View.OnClickListener() { // once send button is pressed do SendNewMessages
            @Override
           public void onClick(View view) {
                Toast.makeText(getApplicationContext(), textMessage.getText(), Toast.LENGTH_LONG).show();
                SendNewMessages();
                GetNewMessages();
            }
        });
    }

    private void setupRecyclerView() {
        mRecyclerView = findViewById(R.id.list_of_messages);
        mAdapter = new SingleChatAdapter();
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    protected void SendNewMessages() {
        JSONObject postData = new JSONObject();
        try {
            postData.put("session", MainActivity.SESSION);
            postData.put("text", textMessage.getText().toString());
            postData.put("chat_id", String.valueOf(ID));
            new SingleChat.displayAllMessages().execute(MainActivity.INEKA_URL + SEND_MESSAGE_URL, postData.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void GetNewMessages(){
        JSONObject postData = new JSONObject();
        try {
            postData.put("session", MainActivity.SESSION);
            postData.put("chat_id", String.valueOf(ID));
            new SingleChat.displayAllMessages().execute(MainActivity.INEKA_URL + GET_ALL_MESSAGES_URL, postData.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

// -------- displayAllMessages must be implemented to onCreate METHOD ------- \\
    // When activity is oppened - recieve all already send messages.

    private class displayAllMessages extends AsyncTask<String, Void, String> {
    @Override
    protected String doInBackground(String... params) {

        String data = "";

        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = (HttpURLConnection) new URL(params[0]).openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.setRequestProperty("Accept", "application/json");
            httpURLConnection.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
            wr.writeBytes(params[1]);
            wr.flush();
            wr.close();

            InputStream in = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(in);

            int inputStreamData = inputStreamReader.read();
            while (inputStreamData != -1) {
                char current = (char) inputStreamData;
                inputStreamData = inputStreamReader.read();
                data += current;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
        return data;
    }
        @Override
        protected void onPostExecute (String data){
            super.onPostExecute(data);
            String serverMessage = null;
            String newGeneratedSession = null;
            Map<String, String> currentObjMap = null;
            ArrayList<Map<String, String>> allObjectsArray = new ArrayList<>();
            try {
                JSONObject jsonObjects = new JSONObject(data);
                try {
                    serverMessage = (String) jsonObjects.get("message");
                } catch (JSONException ignored) {}
                //-------- For "session & message" server answer -----------
                try {
                    newGeneratedSession = (String) jsonObjects.get("session");
                } catch (JSONException ignored) {}
                //-------- For "result & message" server answer -----------
                try {
                    String resultStr = (String) jsonObjects.get("result");
                    resultStr = resultStr.replaceAll("[\\[\\]]", "");
                    String[] arrayObjects = resultStr.split("\\}"); // "\\}"
                    for (String arrayObject : arrayObjects) {
                        String object = arrayObject;
                        if (object.charAt(0) == ',')    // removing , from object beginning 2..3... objects
                            object = object.substring(1);
                        object = object.replaceAll("[\\[\\](){}]", "");
                        String[] pairs = object.split("','");
                        currentObjMap = new HashMap<String, String>();
                        ArrayList<String> keyValue;
                        for (String pair : pairs) {
                            keyValue = new ArrayList<>();
                            pair = pair.replaceAll("[\']", "");
                            if (pair.contains("date_time:")) {
                                //data_time case
                                keyValue.add("date_time");
                                keyValue.add(pair.substring(pair.indexOf(':') + 1));
                                currentObjMap.put(keyValue.get(0), keyValue.get(1));
                            }
                            else {
                                if (pair.contains("path_to_file")) {
                                    keyValue.add("path_to_file");
                                    keyValue.add("");
                                    currentObjMap.put(keyValue.get(0), keyValue.get(1));
                                } else {
                                    if (pair.contains("text")) {
                                        keyValue.add("text");
                                        if (pair.length() > 5) {
                                            keyValue.add(pair.substring(pair.indexOf(':') + 1));
                                        } else {
                                            keyValue.add(" "); // if message is empty (possible for next messages with only file)
                                        }
                                        currentObjMap.put(keyValue.get(0), keyValue.get(1));
                                    } else {
                                        String[] tmp = pair.split(":");
                                        currentObjMap.put(tmp[0], tmp[1]);
                                    }
                                }
                            }
                        }
                        allObjectsArray.add(currentObjMap);
                    }
                    //----- Here, all possible data from "result & message" is collected ------
                    final LinkedList<String> chatsList = new LinkedList<>();
                    for (int i = 0; i <= currentObjMap.size(); i++) {
                        String Text = allObjectsArray.get(i).get("text");
                        chatsList.addLast(Text);
                        mAdapter.updateDataSet(chatsList);
                    }
                } catch (JSONException ignored) {
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }
}