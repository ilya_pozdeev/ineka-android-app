package Android.Ineka.ui.login;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import Android.Ineka.MainActivity;
import Android.Ineka.R;
import Android.Ineka.ui.allChats.AllChats;
import Android.Ineka.ui.home.HomeFragment;
import Android.Ineka.ui.register.RegisterActivity;

public class LoginActivity extends AppCompatActivity {

    private EditText emailEditText;
    private EditText passwordEditText;
    private Button signInButton;
    private Button registerButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        emailEditText = findViewById(R.id.email);
        passwordEditText = findViewById(R.id.password);
        signInButton = (Button) findViewById(R.id.signIn);
        registerButton = (Button) findViewById(R.id.register);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performUserSingIn();
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startRegistrationActivity();
            }
        });
    }

    public void performUserSingIn() {
        JSONObject postData = new JSONObject();
        try {
            postData.put("email", emailEditText.getText().toString());
            postData.put("password", passwordEditText.getText().toString());
            new LoginActivity.LoginUserDetails().execute(MainActivity.INEKA_URL + "/login", postData.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void startRegistrationActivity() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    private class LoginUserDetails extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            String data = "";

            HttpURLConnection httpURLConnection = null;
            try {
                httpURLConnection = (HttpURLConnection) new URL(params[0]).openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/json");
                httpURLConnection.setRequestProperty("Accept", "application/json");
                httpURLConnection.setDoOutput(true);

                DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
                wr.writeBytes(params[1]);
                wr.flush();
                wr.close();

                InputStream in = httpURLConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(in);

                int inputStreamData = inputStreamReader.read();
                while (inputStreamData != -1) {
                    char current = (char) inputStreamData;
                    inputStreamData = inputStreamReader.read();
                    data += current;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            }
            return data;
        }

        @Override
        protected void onPostExecute(String data) {
            super.onPostExecute(data);
            String serverMessage = null;
            String newGeneratedSession = null;
            Map<String, String> currentObjMap = null;
            ArrayList<Map<String, String>> allObjectsArray = new ArrayList<>();
            try {
                JSONObject jsonObjects = new JSONObject(data);
                try {
                    serverMessage = (String) jsonObjects.get("message");
                } catch (JSONException ignored) {
                }
                //-------- For "session & message" server answer -----------
                try {
                    newGeneratedSession = (String) jsonObjects.get("session");
                    if (newGeneratedSession.equals("") || newGeneratedSession.equals(" ")) {
                        Toast.makeText(getApplicationContext(), "Some fields are not filled in", Toast.LENGTH_LONG).show();
                    } else {
                        MainActivity.SESSION = newGeneratedSession;
                        Toast.makeText(getApplicationContext(), "Welcome to InEka!", Toast.LENGTH_LONG).show();
                        startAllChatsActivity();
                    }
                } catch (JSONException ignored) {
                }
                //-------- For "result & message" server answer -----------
                try {
                    String resultStr = (String) jsonObjects.get("result");
                    resultStr = resultStr.replaceAll("[\\[\\]]", "");
                    String[] arrayObjects = resultStr.split("\\}");
                    for (String arrayObject : arrayObjects) {
                        String object = arrayObject;
                        if (object.charAt(0) == ',')    // removing , from object beginning 2..3... objects
                            object = object.substring(1);
                        object = object.replaceAll("[\\[\\](){}]", "");
                        object = object.replaceAll("[\']", "");
                        String[] pairs = object.split(",");
                        currentObjMap = new HashMap<String, String>();
                        for (String pair : pairs) {
                            String[] keyValue = pair.split(":");
                            currentObjMap.put(keyValue[0], keyValue[1]);
                        }
                        allObjectsArray.add(currentObjMap);
                    }

                } catch (JSONException ignored) {
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    private void startAllChatsActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}