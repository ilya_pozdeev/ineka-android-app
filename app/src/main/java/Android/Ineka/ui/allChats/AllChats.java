package Android.Ineka.ui.allChats;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.AsyncTask;
import android.os.Bundle;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import Android.Ineka.MainActivity;
import Android.Ineka.R;

public class AllChats extends AppCompatActivity {
    //String session = "gb8gziPHuecU1swL3@A2w#spT";
    private RecyclerView mRecyclerView;
    private AllChatsAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_chats);
        setupRecyclerView();
        getAllChats();
    }

    private void setupRecyclerView() {
        mRecyclerView = findViewById(R.id.allChatsList);
        mAdapter = new AllChatsAdapter();
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void getAllChats() {
        JSONObject postData = new JSONObject();
        try {
            postData.put("session", MainActivity.SESSION);
            new AllChats.GetAllChats().execute(MainActivity.INEKA_URL + "/get/my/chats", postData.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class GetAllChats extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            String data = "";

            HttpURLConnection httpURLConnection = null;
            try {
                httpURLConnection = (HttpURLConnection) new URL(params[0]).openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/json");
                httpURLConnection.setRequestProperty("Accept", "application/json");
                httpURLConnection.setDoOutput(true);

                DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
                wr.writeBytes(params[1]);
                wr.flush();
                wr.close();

                InputStream in = httpURLConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(in);

                int inputStreamData = inputStreamReader.read();
                while (inputStreamData != -1) {
                    char current = (char) inputStreamData;
                    inputStreamData = inputStreamReader.read();
                    data += current;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            }
            return data;
        }

        @Override
        protected void onPostExecute(String data) {
            super.onPostExecute(data);
            String serverMessage = null;
            String newGeneratedSession = null;
            Map<String, String> currentObjMap = null;
            ArrayList<Map<String, String>> allObjectsArray = new ArrayList<>();
            try {
                JSONObject jsonObjects = new JSONObject(data);
                try {
                    serverMessage = (String) jsonObjects.get("message");
                } catch (JSONException ignored) {}
                //-------- For "session & message" server answer -----------
                try {
                     newGeneratedSession = (String) jsonObjects.get("session");
                } catch (JSONException ignored) {}
                //-------- For "result & message" server answer -----------
                try {
                    String resultStr = (String) jsonObjects.get("result");
                    resultStr = resultStr.replaceAll("[\\[\\]]", "");
                    String[] arrayObjects = resultStr.split("\\}");
                    for (String arrayObject : arrayObjects) {
                        String object = arrayObject;
                        if (object.charAt(0) == ',')    // removing , from object beginning 2..3... objects
                            object = object.substring(1);
                        object = object.replaceAll("[\\[\\](){}]", "");
                        object = object.replaceAll("[\']", "");
                        String[] pairs = object.split(",");
                        currentObjMap = new HashMap<String, String>();
                        for (String pair : pairs) {
                            String[] keyValue = pair.split(":");
                            currentObjMap.put(keyValue[0], keyValue[1]);
                        }
                        allObjectsArray.add(currentObjMap);
                    }
                    //----- Here, all possible data from "result & message" is collected ------
                    final LinkedList<String> chatsList = new LinkedList<>();
                    for (int i = 0; i <= currentObjMap.size(); i++) {
                        String chat_name = allObjectsArray.get(i).get("chat_name");
                        chatsList.addLast(chat_name);
                        mAdapter.updateDataSet(chatsList);
                    }

                } catch (JSONException ignored) {}
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }
}