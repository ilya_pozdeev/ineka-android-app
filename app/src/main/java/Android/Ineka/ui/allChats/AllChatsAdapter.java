package Android.Ineka.ui.allChats;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.LinkedList;

import Android.Ineka.R;

public class AllChatsAdapter extends RecyclerView.Adapter<AllChatsAdapter.ChatViewHolder>  {

    private LinkedList<String> dataSet = new LinkedList<>();


    public void updateDataSet(LinkedList<String> newDataSet) {
        dataSet = newDataSet;
        notifyDataSetChanged();
    }

    @Override
    public AllChatsAdapter.ChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater li = LayoutInflater.from(parent.getContext());
        View mItemView = li.inflate(R.layout.chatlist_item, parent, false);
        return new ChatViewHolder(mItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AllChatsAdapter.ChatViewHolder holder, int position) {
        holder.bind(dataSet.get(position));
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }



    class ChatViewHolder extends RecyclerView.ViewHolder {

        public final TextView wordItemView;


        public ChatViewHolder(View itemView) {
            super(itemView);
            wordItemView = itemView.findViewById(R.id.chat);
        }

        public void bind(String text) {
            wordItemView.setText(text);
        }
    }
}
