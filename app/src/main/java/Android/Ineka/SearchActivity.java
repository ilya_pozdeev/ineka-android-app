package Android.Ineka;
// Konstantin
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.SwitchCompat;

import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Android.Ineka.ui.ChatFragments.SingleChat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchActivity extends AppCompatActivity {

    String name, surname, sessionNr;
    EditText nameInput;
    EditText surnameInput;
    String result;
    private RequestQueue mQueue;
    public static String globalEmail = "";

    private TextView textViewResult;
    private TextView textViewResultTwo;
    private TextView textViewResultThree;
    private JsonPlaceHolderApi jsonPlaceHolderApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        nameInput = (EditText) findViewById(R.id.searchTextPersonName);
        surnameInput = (EditText) findViewById(R.id.searchTextPersonSurname);
        mQueue = Volley.newRequestQueue(this);

    }

    public void onClickSearchButton(View view) {

        name = nameInput.getText().toString();
        surname = surnameInput.getText().toString();

        textViewResult = findViewById(R.id.text_view_search_result);
        textViewResultTwo = findViewById(R.id.text_view_search_result_two);
        textViewResultThree = findViewById(R.id.text_view_search_result_three);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.INEKA_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        result = "Name: " + name + " Surname: " + surname;
//        Toast.makeText(SearchActivity.this, result, Toast.LENGTH_SHORT).show();


        createSearchPost();
    }



    private void createSearchPost() {

//        String url = "https://run.mocky.io/v3/bc7485ae-e92c-441a-acd2-a587e3ca88c1";
        String url = MainActivity.INEKA_URL + "/user/find/";

        JSONObject postData = new JSONObject();
        try {
            postData.put("session", "gb8gziPHuecU1swL3@A2w#spT"); // MainActivity.SESSION
            postData.put("name", name);
            postData.put("surname", surname);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, postData,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String jsonString = response.toString();

//                        {"result":"[{"id":9, "name":"Lara", "surname":"Mazite", "email":"test1@gmail.com", "password":"fyghbujnimk123"}]","message":"Found 1 users"}

                        // String jsonString = yourServerAnswer..
                        Map<String, String> currentObjMap = null;
                        ArrayList<Map<String, String>> allObjectsArray = new ArrayList<>();
                        String serverMessage = null;
                        String newGeneratedSession = null;

                        try {
                            JSONObject jsonObjects = new JSONObject(jsonString);

                            try {
                                serverMessage = (String) jsonObjects.get("message");
                            } catch (JSONException ignored) {}
                            //-------- For "session & message" server answer -----------
                            try {
                                newGeneratedSession = (String) jsonObjects.get("session");
                            } catch (JSONException ignored) {}
                            //-------- For "result & message" server answer -----------
                            try {
                                String resultStr = (String) jsonObjects.get("result");
                                resultStr = resultStr.replaceAll("[\\[\\]]", "");
                                String[] arrayObjects = resultStr.split("\\}");
                                for (String arrayObject : arrayObjects) {
                                    String object = arrayObject;
                                    if (object.charAt(0) == ',')    // removing , from object beginning 2..3... objects
                                        object = object.substring(1);
                                    object = object.replaceAll("[\\[\\](){}]", "");
                                    object = object.replaceAll("[\']", "");
                                    String[] pairs = object.split(",");
                                    currentObjMap = new HashMap<String, String>();
                                    for (String pair : pairs) {
                                        String[] keyValue = pair.split(":");
                                        currentObjMap.put(keyValue[0], keyValue[1]);
                                    }
                                    allObjectsArray.add(currentObjMap);
                                }
                                //----- Here, all possible data from "result & message" is collected ------
                                //TODO: write your code here, to get necessary fields, in this way:
                                // String id_recipient = allObjectsArray.get(0).get("id_recipient");

                                textViewResult.append("Search results: \n" +
                                        "Id: " + allObjectsArray.get(0).get("id")
                                        + "\nname: " + allObjectsArray.get(0).get("name")
                                        + "\nsurname: " + allObjectsArray.get(0).get("surname")
                                        + "\nemail: " + allObjectsArray.get(0).get("email"));

                                globalEmail = allObjectsArray.get(0).get("email");

                                textViewResultTwo.append(
                                        "Id: " + allObjectsArray.get(1).get("id")
                                                + "\nname: " + allObjectsArray.get(1).get("name")
                                                + "\nsurname: " + allObjectsArray.get(1).get("surname")
                                                + "\nemail: " + allObjectsArray.get(1).get("email"));

                                textViewResultThree.append(
                                        "Id: " + allObjectsArray.get(2).get("id")
                                                + "\nname: " + allObjectsArray.get(2).get("name")
                                                + "\nsurname: " + allObjectsArray.get(2).get("surname")
                                                + "\nemail: " + allObjectsArray.get(2).get("email"));

                                int i = 0; // YOU CAN ADD HERE BREAKPOINT to check "result" convertation to Array
                            } catch (JSONException ignored) {}
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }

//                        try {
//                            JSONObject jsnobject = new JSONObject(response.toString());
//                            JSONArray jsonArray = jsnobject.getJSONArray("result");
//                            for (int i = 0; i < jsonArray.length(); i++) {
//                                JSONObject explrObject = jsonArray.getJSONObject(i);
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                            JSONArray jsonArray = response.getJSONArray();
//                            System.out.println(jsonArray);
//                            String stringResult = response.getJSONArray("result").toString();
//                            System.out.println(stringResult);
//                            JSONArray jsonArray = null;

//                            for (int i = 0; i < jsonArray.length(); i++) {
//                                JSONObject messages = jsonArray.getJSONObject(i);
//
//                                String name = messages.getString("name");
//                                String message = messages.getString("message");
//                                int chatRoomId = messages.getInt("chat_name");
////                                TimeUnit.SECONDS.sleep(1);
//
//                                mTextViewResult.append(name + ": " + message + " (chat_name: " + String.valueOf(chatRoomId)  + ")\n\n");
//                            }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        mQueue.add(request);
    }

    public JSONObject onClickSearchResult(View view) {
//        Toast.makeText(getApplicationContext(), "CLICK",Toast.LENGTH_LONG).show();

        JSONObject postData = new JSONObject();
        System.out.println(this);
        try {
            postData.put("session", MainActivity.SESSION);
            postData.put("receiver_email", globalEmail);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(getApplicationContext(), SingleChat.class);
        startActivity(intent);
        return postData;
    }

    // Prev version

//    private void createSearchPost() {
//        Post post = new Post("gb8gziPHuecU1swL3@A2w#spT", name, surname);
//
//        Call<Post> call = jsonPlaceHolderApi.createSearchPost(post);
//
//        call.enqueue(new Callback<Post>() {
//            @Override
//            public void onResponse(Call<Post> call, Response<Post> response) {
//                Post postResponse = response.body();
//
//                if (!response.isSuccessful()) {
//                    textViewResult.setText("Code: " + response.code());
//                    return;
//                }
////                    MainActivity.SESSION = postResponse.getSession();
//
//                    String jsonString = postResponse.getResult();
//
//                // String jsonString = yourServerAnswer..
//                Map<String, String> currentObjMap = null;
//                ArrayList<Map<String, String>> allObjectsArray = new ArrayList<>();
//                String serverMessage = null;
//                String newGeneratedSession = null;
//
//                try {
//                    JSONObject jsonObjects = new JSONObject(jsonString);
//
//                    try {
//                        serverMessage = (String) jsonObjects.get("message");
//                    } catch (JSONException ignored) {}
//                    //-------- For "session & message" server answer -----------
//                    try {
//                        newGeneratedSession = (String) jsonObjects.get("session");
//                    } catch (JSONException ignored) {}
//                    //-------- For "result & message" server answer -----------
//                    try {
//                        String resultStr = (String) jsonObjects.get("result");
//                        resultStr = resultStr.replaceAll("[\\[\\]]", "");
//                        String[] arrayObjects = resultStr.split("\\}");
//                        for (String arrayObject : arrayObjects) {
//                            String object = arrayObject;
//                            if (object.charAt(0) == ',')    // removing , from object beginning 2..3... objects
//                                object = object.substring(1);
//                            object = object.replaceAll("[\\[\\](){}]", "");
//                            object = object.replaceAll("[\']", "");
//                            String[] pairs = object.split(",");
//                            currentObjMap = new HashMap<String, String>();
//                            for (String pair : pairs) {
//                                String[] keyValue = pair.split(":");
//                                currentObjMap.put(keyValue[0], keyValue[1]);
//                            }
//                            allObjectsArray.add(currentObjMap);
//                        }
//                        //----- Here, all possible data from "result & message" is collected ------
//                        //TODO: write your code here, to get necessary fields, in this way:
//
//                        String id = allObjectsArray.get(0).get("id");
//                        String name = allObjectsArray.get(0).get("name");
//                        String surname = allObjectsArray.get(0).get("surname");
//                        String email = allObjectsArray.get(0).get("email");
//                        String message = allObjectsArray.get(0).get("message");
//
//                        System.out.println("id: " + id + "name: " + name + "surname: " + surname);
//
//                        textViewResult.setText("id: " + id + "name: " + name + "surname: " + surname);
//
//                        //{"result":"[{"id":9, "name":"Lara", "surname":"Mazite", "email":"test1@gmail.com", "password":"fyghbujnimk123"}]","message":"Found 1 users"}
//                        int i = 0; // YOU CAN ADD HERE BREAKPOINT to check "result" convertation to Array
//                    } catch (JSONException ignored) {}
//                } catch (Throwable e) {
//                    e.printStackTrace();
//                }
//
//
//
//
//
//                    //{"result":"[{"id":9, "name":"Lara", "surname":"Mazite", "email":"test1@gmail.com", "password":"fyghbujnimk123"}]","message":"Found 1 users"}
//
////                    content += "Result: " + postResponse.getResult() + "\n";
////                content += "Message: " + postResponse.getMessage() + "\n";
//
////                System.out.println("!!!!!!!!!!!!!!!!!!" + postResponse.getSession() + "MainActivity session: " + MainActivity.SESSION);
////                content += "Email: " + postResponse.getEmail() + "\n";
////                content += "Text: " + postResponse.getText() + "\n\n";
//
//            }
//
//            @Override
//            public void onFailure(Call<Post> call, Throwable t) {
//                textViewResult.setText(t.getMessage());
//            }
//        });
//
//    }

}